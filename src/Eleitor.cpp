#include "Eleitor.hpp"
#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
using namespace std;

Eleitor::Eleitor(){
  nome_eleitor = "Branco";
  deputado_distrital = "Branco";
  deputado_federal = "Branco";
  prim_senador = "Branco";
  seg_senador = "Branco";
  governador = "Branco";
  presidente = "Branco";
}
Eleitor::~Eleitor(){}

void Eleitor::set_nome_eleitor(string nome){
  nome_eleitor = nome;
}
string Eleitor::get_nome_eleitor(){
  return nome_eleitor;
}

void Eleitor::set_deputado_distrital(string esc_dpt_dist){
  deputado_distrital = esc_dpt_dist;
}
string Eleitor::get_deputado_distrital(){
  return deputado_distrital;
}

void Eleitor::set_deputado_federal(string esc_dpt_fed){
  deputado_federal = esc_dpt_fed;
}
string Eleitor::get_deputado_federal(){
  return deputado_federal;
}

void Eleitor::set_prim_senador(string esc_prim_sen){
  prim_senador = esc_prim_sen;
}
string Eleitor::get_prim_senador(){
  return prim_senador;
}

void Eleitor::set_seg_senador(string esc_seg_sen){
  seg_senador = esc_seg_sen;
}
string Eleitor::get_seg_senador(){
  return seg_senador;
}

void Eleitor::set_governador(string esc_gov){
  governador = esc_gov;
}
string Eleitor::get_governador(){
  return governador;
}

void Eleitor::set_presidente(string esc_pres){
  presidente = esc_pres;
}
string Eleitor::get_presidente(){
  return presidente;
}
