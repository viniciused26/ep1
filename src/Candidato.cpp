#include "Candidato.hpp"
#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
using namespace std;

Candidato::Candidato(){
  votos = 0;
}
Candidato::~Candidato(){}

void Candidato::set_votos(int votos){
  votos = votos;
}
int Candidato::get_votos(){
  return votos;
}

void Candidato::set_dados_candidato_brasil(UrnaEleitoral urna, int num_candidato){
  int n_nm_ue, n_ds_cargo, n_nr_candidato, n_nm_urna_candidato, n_nm_partido;

  n_nm_ue = 12 + (57*num_candidato);
  n_ds_cargo = 14 + (57*num_candidato);
  n_nr_candidato = 16 + (57*num_candidato);
  n_nm_urna_candidato = 18 + (57*num_candidato);
  n_nm_partido = 29 + (57*num_candidato);

  nm_ue = urna.get_dados_brasil(n_nm_ue);
  ds_cargo = urna.get_dados_brasil(n_ds_cargo);
  nr_candidato = urna.get_dados_brasil(n_nr_candidato);
  nm_urna_candidato = urna.get_dados_brasil(n_nm_urna_candidato);
  nm_partido = urna.get_dados_brasil(n_nm_partido);
}

void Candidato::set_dados_candidato_df(UrnaEleitoral urna, int num_candidato){
  int n_nm_ue, n_ds_cargo, n_nr_candidato, n_nm_urna_candidato, n_nm_partido;

  n_nm_ue = 12 + (57*num_candidato);
  n_ds_cargo = 14 + (57*num_candidato);
  n_nr_candidato = 16 + (57*num_candidato);
  n_nm_urna_candidato = 18 + (57*num_candidato);
  n_nm_partido = 29 + + (57*num_candidato);

  nm_ue = urna.get_dados_df(n_nm_ue);
  ds_cargo = urna.get_dados_df(n_ds_cargo);
  nr_candidato = urna.get_dados_df(n_nr_candidato);
  nm_urna_candidato = urna.get_dados_df(n_nm_urna_candidato);
  nm_partido = urna.get_dados_df(n_nm_partido);
}

string Candidato::get_nm_ue_candidato_brasil(){
  return nm_ue;
}
string Candidato::get_ds_cargo_candidato_brasil(){
  return ds_cargo;
}
string Candidato::get_nr_candidato_brasil(){
  return nr_candidato;
}
string Candidato::get_nm_urna_candidato_brasil(){
  return nm_urna_candidato;
}
string Candidato::get_nm_partido_candidato_brasil(){
  return nm_partido;
}

string Candidato::get_nm_ue_candidato_df(){
  return nm_ue;
}
string Candidato::get_ds_cargo_candidato_df(){
  return ds_cargo;
}
string Candidato::get_nr_candidato_df(){
  return nr_candidato;
}
string Candidato::get_nm_urna_candidato_df(){
  return nm_urna_candidato;
}
string Candidato::get_nm_partido_candidato_df(){
  return nm_partido;
}

void Candidato::contagem_de_votos(int qnt_eleitores, Eleitor *eleitores){
  int eleit;

  for(eleit = 0; eleit < qnt_eleitores; eleit++){
    if(nm_urna_candidato.compare(eleitores[eleit].get_deputado_distrital()) == 0){
      votos += 1;
    }else if(nm_urna_candidato.compare(eleitores[eleit].get_deputado_federal()) == 0){
      votos += 1;
    }else if(nm_urna_candidato.compare(eleitores[eleit].get_prim_senador()) == 0){
      votos += 1;
    }else if(nm_urna_candidato.compare(eleitores[eleit].get_seg_senador()) == 0){
      votos += 1;
    }else if(nm_urna_candidato.compare(eleitores[eleit].get_governador()) == 0){
      votos += 1;
    }else if(nm_urna_candidato.compare(eleitores[eleit].get_presidente()) == 0){
      votos += 1;
    }
  }
}
