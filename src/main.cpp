#include <iostream>
#include "UrnaEleitoral.hpp"
#include "Candidato.hpp"
using namespace std;

int main() {
  int br;//Variáveis de contagem de candidatos comuns para o Brasil
  int df;//Variáveis de contagem de candidatos comuns para eleitores do Distrito

  int qnt_eleitores;

  int opc;
  bool saida = false;

  string nome_temp;
  string dpt_dist_temp;
  string dpt_est_temp;
  string prim_sen_temp;
  string seg_sen_temp;
  string gov_temp;
  string pres_temp;

  UrnaEleitoral urna;//Inicializando objeto urna

  urna.set_dados_brasil();//Lendo arquivo de candidatos comuns para o Brasil
  urna.set_dados_df();//Lendo arquivo de candidatos comuns para eleitores do Distrito Federal

  Candidato candidatos_brasil[27];//Inicializando objetos candidatos comuns para o Brasil
  Candidato candidatos_df[1238];//Inicializando objetos candidatos comuns para eleitores do Distrito Federal

  for(br = 1; br < 27; br++){
    candidatos_brasil[br].set_dados_candidato_brasil(urna, br);//Coletando dados dos candidatos comuns para o Brasil e origanizando-os
  }
  for(df = 1; df < 1238; df++){
    candidatos_df[df].set_dados_candidato_df(urna, df);//Coletando dados dos candidatos comuns para eleitores do Distrito Federal e origanizando-os
  }

  cout << "Quantos eleitores irão votar?" << endl;
  cin >> qnt_eleitores;
  Eleitor *eleitores = new Eleitor[qnt_eleitores];
  urna.set_quantidade_eleitores(qnt_eleitores);

  urna.votacao(eleitores);



  for(br = 1; br < 27; br++){
    candidatos_brasil[br].contagem_de_votos(qnt_eleitores, eleitores);
  }
  for(df = 1; df < 1238; df++){
    candidatos_df[df].contagem_de_votos(qnt_eleitores, eleitores);
  }

  int voto_distrital_temp = -1;
  int voto_federal_temp = -1;
  int voto_prim_sen_temp = -1;
  //int voto_seg_sen_temp = -1;
  int voto_gov_temp = -1;
  int voto_pres_temp = -1;

  string distrital_maior;
  string federal_maior;
  string prim_sen_maior;
  string seg_sen_maior;
  string governador_maior;
  string presidente_maior;

  for(br = 1; br < 27; br++){
    if(candidatos_brasil[br].get_ds_cargo_candidato_brasil() == "\"PRESIDENTE\""){
      if(candidatos_brasil[br].get_votos() > voto_pres_temp){
        voto_pres_temp = candidatos_brasil[br].get_votos();
        presidente_maior = candidatos_brasil[br].get_nm_urna_candidato_df();
      }
    }
  }

  for(df = 1; df < 1238; df++){
    if(candidatos_df[df].get_ds_cargo_candidato_df() == "\"DEPUTADO DISTRITAL\""){
      if(candidatos_df[df].get_votos() > voto_distrital_temp){
        voto_distrital_temp = candidatos_df[df].get_votos();
        distrital_maior = candidatos_df[df].get_nm_urna_candidato_df();
      }
    }else if(candidatos_df[df].get_ds_cargo_candidato_df() == "\"DEPUTADO FEDERAL\""){
      if(candidatos_df[df].get_votos() > voto_federal_temp){
        voto_federal_temp = candidatos_df[df].get_votos();
        federal_maior = candidatos_df[df].get_nm_urna_candidato_df();
      }
    }else if(candidatos_df[df].get_ds_cargo_candidato_df() == "\"SENADOR\""){
      if(candidatos_df[df].get_votos() > voto_prim_sen_temp){
        voto_prim_sen_temp = candidatos_df[df].get_votos();
        prim_sen_maior = candidatos_df[df].get_nm_urna_candidato_df();
      }
    }else if(candidatos_df[df].get_ds_cargo_candidato_df() == "\"GOVERNADOR\""){
      if(candidatos_df[df].get_votos() > voto_gov_temp){
        voto_gov_temp = candidatos_df[df].get_votos();
        governador_maior = candidatos_df[df].get_nm_urna_candidato_df();
      }
    }
  }


  cout << "Obrigado por participar das eleições!" << endl;

  do{
    cout << "\nCaso você queira ver o resultado digite 1, caso você queira ver o relatório de votos digite 2, se você quiser sair digite 3." << endl;
    cin >> opc;
    if(opc == 1){
      cout << "\n----------------------" << endl;
      cout << "\nVencedor para presidente: " << presidente_maior;
      cout << "\nVencedor para governador: " << governador_maior;
      cout << "\nVencedor para senador: " << prim_sen_maior;
      cout << "\nVencedor para deputado federal: " << federal_maior;
      cout << "\nVencedor para deputado_distrital: " << distrital_maior;
      saida = false;
    }else if(opc == 2){
      urna.relatorio(eleitores);
      saida = false;
    }else if(opc == 3){
      saida = true;
    }else{
      cout << "Por favor insira um número válido." << endl;
    }
  }while(saida == false);









  return 0;
}
