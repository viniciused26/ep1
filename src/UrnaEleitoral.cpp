#include "UrnaEleitoral.hpp"
#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
using namespace std;

UrnaEleitoral::UrnaEleitoral(){}
UrnaEleitoral::~UrnaEleitoral(){}

void UrnaEleitoral::set_dados_brasil(){
  string leitura;
  int i;

  ifstream arquivo("data/consulta_cand_2018_BR.csv");

  if(!arquivo.is_open()) std::cout << "Erro: Não foi possível abrir o arquivo." << "\n";

  while(arquivo.good()){
    for(i = 0; i < 1652; i++){
      getline(arquivo, leitura, ';');
      dados_brasil[i] = leitura;
    }
  }
}

string UrnaEleitoral::get_dados_brasil(int dado){
  int i = dado;
  return dados_brasil[i];
}

void UrnaEleitoral::set_dados_df(){
  string leitura;
  int i;

  ifstream arquivo("data/consulta_cand_2018_DF.csv");

  if(!arquivo.is_open()) std::cout << "Erro: Não foi possível abrir o arquivo." << "\n";

  while(arquivo.good()){
    for(i = 0; i < 73101; i++){
      getline(arquivo, leitura, ';');
      dados_df[i] = leitura;
    }
  }
}

string UrnaEleitoral::get_dados_df(int dado){
  int i = dado;
  return dados_df[i];
}

void UrnaEleitoral::set_quantidade_eleitores(int eleitores){
  quantidade_eleitores = eleitores;
}

int UrnaEleitoral::get_quantidade_eleitores(){
  return quantidade_eleitores;
}

void UrnaEleitoral::votacao(Eleitor *eleitores){
  int eleit;
  int ordem_eleitores;

  int branco_temp;

  int perc;//Percorre dados

  bool encontrou_dpt_dist = false;
  int dpt_dist_conf = 2;

  bool encontrou_dpt_fed = false;
  int dpt_fed_conf = 2;

  bool encontrou_prim_sen = false;
  int prim_sen_conf = 2;

  bool encontrou_seg_sen = false;
  int seg_sen_conf = 2;

  bool encontrou_gov = false;
  int gov_conf = 2;


  bool encontrou_pres = false;//Encontrou o presidente
  int pres_conf = 2;

  string nome_temp;
  string dpt_dist_temp;
  string dpt_fed_temp;
  string prim_sen_temp;
  string seg_sen_temp;
  string gov_temp;
  string pres_temp;

  string valid;

  for(eleit = 0; eleit < quantidade_eleitores; eleit++){
    ordem_eleitores = eleit + 1;
    cout << "\n" << "Qual o nome do eleitor número " << ordem_eleitores << "?" << endl;
    cin >> nome_temp;
    eleitores[eleit].set_nome_eleitor(nome_temp);


    //Votação para deputado distrital
    do{
      cout << "\n" << "# Votação deputado distrital." << endl;
      cout << "O que você deseja fazer?" << "\n\n" << "1. Digitar número do candidato." << "\n" << "2. Votar em branco." << endl;
      cin >> branco_temp;

      do{
        if(branco_temp == 1){
          do{
            cout << "\n" << "Digite o número do candidato que você deseja eleger para deputado distrital." << endl;
            cin >> dpt_dist_temp;
            valid = "\"" + dpt_dist_temp + "\"";

            for(perc = 0; perc < 73101; perc++){
              if(valid == dados_df[perc]){
                if(dados_df[perc-2] == "\"DEPUTADO DISTRITAL\""){
                  encontrou_dpt_dist = true;
                  break;
                }
              }
            }

            if(encontrou_dpt_dist == true){
              cout << "\n" << "Você deseja votar no candidato " << dados_df[perc + 2] << " de número " << dados_df[perc] << " do partido " << dados_df[perc + 13] << "?" << endl;
            }else if(encontrou_dpt_dist == false){
              cout << "\n" << "Nenhum candidato encontrado. Por favor digite novamente." << endl;
            }
          }while(encontrou_dpt_dist == false);

          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> dpt_dist_conf;

          if(dpt_dist_conf == 1){
            eleitores[eleit].set_deputado_distrital(dados_df[perc + 2]);
          }

        }else if(branco_temp == 2){
          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> dpt_dist_conf;
        }else{
          cout << "\n" << "Número inválido. Digite 1 para voltar a votação." << endl;
          cin >> branco_temp;
        }
      }while (branco_temp != 1 && branco_temp != 2);
    }while(dpt_dist_conf == 2);

    //Votação para deputado estadual
    do{
      cout << "\n" << "# Votação deputado federal." << endl;
      cout << "O que você deseja fazer?" << "\n\n" << "1. Digitar número do candidato." << "\n" << "2. Votar em branco." << endl;
      cin >> branco_temp;

      do{
        if(branco_temp == 1){
          do{
            cout << "\n" << "Digite o número do candidato que você deseja eleger para deputado federal." << endl;
            cin >> dpt_fed_temp;
            valid = "\"" + dpt_fed_temp + "\"";

            for(perc = 0; perc < 73101; perc++){
              if(valid == dados_df[perc]){
                if(dados_df[perc-2] == "\"DEPUTADO FEDERAL\""){
                  encontrou_dpt_fed = true;
                  break;
                }
              }
            }

            if(encontrou_dpt_fed == true){
              cout << "\n" << "Você deseja votar no candidato " << dados_df[perc + 2] << " de número " << dados_df[perc] << " do partido " << dados_df[perc + 13] << "?" << endl;
            }else if(encontrou_dpt_fed == false){
              cout << "\n" << "Nenhum candidato encontrado. Por favor digite novamente." << endl;
            }
          }while(encontrou_dpt_fed == false);

          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> dpt_fed_conf;

          if(dpt_fed_conf == 1){
            eleitores[eleit].set_deputado_federal(dados_df[perc + 2]);
          }

        }else if(branco_temp == 2){
          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> dpt_fed_conf;
        }else{
          cout << "\n" << "Número inválido. Digite 1 para voltar a votação." << endl;
          cin >> branco_temp;
        }
      }while (branco_temp != 1 && branco_temp != 2);

    }while(dpt_fed_conf == 2);

    //Votação para primeiro senador
    do{
      cout << "\n" << "# Votação para o primeiro senador." << endl;
      cout << "O que você deseja fazer?" << "\n\n" << "1. Digitar número do candidato." << "\n" << "2. Votar em branco." << endl;
      cin >> branco_temp;

      do{
        if(branco_temp == 1){
          do{
            cout << "\n" << "Digite o número do candidato que você deseja eleger para primeiro senador." << endl;
            cin >> prim_sen_temp;
            valid = "\"" + prim_sen_temp + "\"";

            for(perc = 0; perc < 73101; perc++){
              if(valid == dados_df[perc]){
                if(dados_df[perc-2] == "\"SENADOR\""){
                  encontrou_prim_sen = true;
                  break;
                }
              }
            }

            if(encontrou_prim_sen == true){
              cout << "\n" << "Você deseja votar no candidato " << dados_df[perc + 2] << " de número " << dados_df[perc] << " do partido " << dados_df[perc + 13] << "?" << endl;
            }else if(encontrou_prim_sen == false){
              cout << "\n" << "Nenhum candidato encontrado. Por favor digite novamente." << endl;
            }
          }while(encontrou_prim_sen == false);

          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> prim_sen_conf;

          if(prim_sen_conf == 1){
            eleitores[eleit].set_prim_senador(dados_df[perc + 2]);
          }

        }else if(branco_temp == 2){
          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> prim_sen_conf;
        }else{
          cout << "\n" << "Número inválido. Digite 1 para voltar a votação." << endl;
          cin >> branco_temp;
        }
      }while (branco_temp != 1 && branco_temp != 2);

    }while(prim_sen_conf == 2);

    //Votação para segundo senador
    do{
      cout << "\n" << "# Votação para o segundo senador." << endl;
      cout << "O que você deseja fazer?" << "\n\n" << "1. Digitar número do candidato." << "\n" << "2. Votar em branco." << endl;
      cin >> branco_temp;

      do{
        if(branco_temp == 1){
          do{
            cout << "\n" << "Digite o número do candidato que você deseja eleger para segundo senador." << endl;
            cin >> seg_sen_temp;
            do{
              if(seg_sen_temp.compare(prim_sen_temp) == 0){
              cout << "\n" << "Você já elegeu esse candidato. Por favor insira outro candidato" << endl;
              cin >> seg_sen_temp;
              }
            }while(seg_sen_temp.compare(prim_sen_temp) == 0);

            valid = "\"" + seg_sen_temp + "\"";

            for(perc = 0; perc < 73101; perc++){
              if(valid == dados_df[perc]){
                if(dados_df[perc-2] == "\"SENADOR\""){
                  encontrou_seg_sen = true;
                  break;
                }
              }
            }

            if(encontrou_seg_sen == true){
              cout << "\n" << "Você deseja votar no candidato " << dados_df[perc + 2] << " de número " << dados_df[perc] << " do partido " << dados_df[perc + 13] << "?" << endl;
            }else if(encontrou_seg_sen == false){
              cout << "\n" << "Nenhum candidato encontrado. Por favor digite novamente." << endl;
            }
          }while(encontrou_seg_sen == false);

          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> seg_sen_conf;

          if(seg_sen_conf == 1){
            eleitores[eleit].set_seg_senador(dados_df[perc + 2]);
          }

        }else if(branco_temp == 2){
          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> seg_sen_conf;
        }else{
          cout << "\n" << "Número inválido. Digite 1 para voltar a votação." << endl;
          cin >> branco_temp;
        }
      }while (branco_temp != 1 && branco_temp != 2);

    }while(seg_sen_conf == 2);

    //Votação para governador
    do{
      cout << "\n" << "# Votação para o governador." << endl;
      cout << "O que você deseja fazer?" << "\n\n" << "1. Digitar número do candidato." << "\n" << "2. Votar em branco." << endl;
      cin >> branco_temp;

      do{
        if(branco_temp == 1){
          do{
            cout << "\n" << "Digite o número do candidato que você deseja eleger para governador." << endl;
            cin >> gov_temp;
            valid = "\"" + gov_temp + "\"";

            for(perc = 0; perc < 73101; perc++){
              if(valid == dados_df[perc]){
                if(dados_df[perc-2] == "\"GOVERNADOR\""){
                  encontrou_gov = true;
                  break;
                }
              }
            }

            if(encontrou_gov == true){
              cout << "\n" << "Você deseja votar no candidato " << dados_df[perc + 2] << " de número " << dados_df[perc] << " do partido " << dados_df[perc + 13] << "?" << endl;
            }else if(encontrou_gov == false){
              cout << "\n" << "Nenhum candidato encontrado. Por favor digite novamente." << endl;
            }
          }while(encontrou_gov == false);

          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> gov_conf;

          if(gov_conf == 1){
            eleitores[eleit].set_governador(dados_df[perc + 2]);
          }

        }else if(branco_temp == 2){
          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> gov_conf;
        }else{
          cout << "\n" << "Número inválido. Digite 1 para voltar a votação." << endl;
          cin >> branco_temp;
        }
      }while (branco_temp != 1 && branco_temp != 2);

    }while(gov_conf == 2);

    //Votação para presidente
    do{
      cout << "\n" << "# Votação para presidente." << endl;
      cout << "\n" << "O que você deseja fazer?" << "\n\n" << "1. Digitar número do candidato." << "\n" << "2. Votar em branco." << endl;
      cin >> branco_temp;

      do{
        if(branco_temp == 1){
          do{
            cout << "\n" << "Digite o número do candidato que você deseja eleger para presidente." << endl;
            cin >> pres_temp;
            valid = "\"" + pres_temp + "\"";

            for(perc = 0; perc < 1652; perc++){
              if(valid == dados_brasil[perc]){
                if(dados_brasil[perc-2] == "\"PRESIDENTE\""){
                  encontrou_pres = true;
                  break;
                }
              }
            }

            if(encontrou_pres == true){
              cout << "\n" << "Você deseja votar no candidato " << dados_brasil[perc + 2] << " de número " << dados_brasil[perc] << " do partido " << dados_brasil[perc + 13] << "?" << endl;
            }else if(encontrou_pres == false){
              cout << "\n" << "Nenhum candidato encontrado. Por favor digite novamente." << endl;
            }
          }while(encontrou_pres == false);

          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> pres_conf;

          if(pres_conf == 1){
            eleitores[eleit].set_presidente(dados_brasil[perc + 2]);
          }

        }else if(branco_temp == 2){
          cout << "\n" << "Você confirma seu voto?" << endl;
          cout << "1. Confirmar." << "\n" << "2. Cancelar." << endl;
          cin >> pres_conf;
        }else{
          cout << "\n" << "Número inválido. Digite 1 para voltar a votação." << endl;
          cin >> branco_temp;
        }
      }while (branco_temp != 1 && branco_temp != 2);

    }while(pres_conf == 2);

  }
}

void UrnaEleitoral::relatorio(Eleitor *eleitores){
  int eleit;

  for(eleit = 0; eleit < quantidade_eleitores; eleit++){
    cout << "\n----------------------------" << endl;
    cout << "\nEleitor número " << eleit + 1 << ": " << eleitores[eleit].get_nome_eleitor() << endl;
    cout << "\nDeputado distrital: " << eleitores[eleit].get_deputado_distrital() << endl;
    cout << "\nDeputado federal: " << eleitores[eleit].get_deputado_federal() << endl;
    cout << "\nPrimeiro senador: " << eleitores[eleit].get_prim_senador() << endl;
    cout << "\nSegundo senador: " << eleitores[eleit].get_seg_senador() << endl;
    cout << "\nGovernador: " << eleitores[eleit].get_governador() << endl;
    cout << "\nPresidente: " << eleitores[eleit].get_presidente() << endl;
  }


}
