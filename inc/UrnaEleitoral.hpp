#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
#include "Eleitor.hpp"
using namespace std;

class UrnaEleitoral{
  private:
    string dados_brasil[1652];
    string dados_df[73101];
    int quantidade_eleitores;

  public:
    UrnaEleitoral();
    ~UrnaEleitoral();

    void set_dados_brasil();
    string get_dados_brasil(int dado);

    void set_dados_df();
    string get_dados_df(int dado);

    void set_quantidade_eleitores(int eleitores);
    int get_quantidade_eleitores();

    void votacao(Eleitor *eleitores);

    void relatorio(Eleitor *eleitores);
};
