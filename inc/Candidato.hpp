#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
using namespace std;

class Candidato{
  private:
    string nm_ue;//12
    string ds_cargo;//14
    string nr_candidato;//16
    string nm_urna_candidato;//18
    string nm_partido;//29
    int votos;

  public:
    Candidato();
    ~Candidato();

    void set_votos(int votos);
    int get_votos();

    void set_dados_candidato_brasil(UrnaEleitoral urna, int num_candidato);
    void set_dados_candidato_df(UrnaEleitoral urna, int num_candidato);

    string get_nm_ue_candidato_brasil();
    string get_ds_cargo_candidato_brasil();
    string get_nr_candidato_brasil();
    string get_nm_urna_candidato_brasil();
    string get_nm_partido_candidato_brasil();

    string get_nm_ue_candidato_df();
    string get_ds_cargo_candidato_df();
    string get_nr_candidato_df();
    string get_nm_urna_candidato_df();
    string get_nm_partido_candidato_df();

    void contagem_de_votos(int qnt_eleitores, Eleitor *eleitores);
};
