#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
using namespace std;

class Eleitor{
  private:
    string nome_eleitor;
    string deputado_distrital;
    string deputado_federal;
    string prim_senador;
    string seg_senador;
    string governador;
    string presidente;

  public:
    Eleitor();
    ~Eleitor();

    void set_nome_eleitor(string nome);
    string get_nome_eleitor();

    void set_deputado_distrital(string esc_dpt_dist);
    string get_deputado_distrital();

    void set_deputado_federal(string esc_dpt_est);
    string get_deputado_federal();

    void set_prim_senador(string esc_prim_sen);
    string get_prim_senador();

    void set_seg_senador(string esc_seg_sen);
    string get_seg_senador();

    void set_governador(string esc_gov);
    string get_governador();

    void set_presidente(string esc_pres);
    string get_presidente();
};
